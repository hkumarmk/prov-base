Facter.add(:ipaddress_brex_or_eth1) do
  setcode do
    Facter.value(:ipaddress_br_ex) || Facter.value(:ipaddress_eth1)
  end
end
