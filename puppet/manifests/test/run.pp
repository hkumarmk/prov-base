#
# Define prov::profiles::test::run
# == To run validation checks to make sure the service is up
#
define prov::profiles::test::run {
  exec {"/usr/lib/prov/tests/${name}": }
}
