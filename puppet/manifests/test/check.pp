#
# Class: prov_base::test::check
#

define prov_base::test::check(
  $port    = 0,
  $address = '127.0.0.1',
  $ssl     = false,
  $type    = 'http',
) {

  include prov_base::test::base

  file { "/usr/lib/prov/tests/${name}.sh":
    content => template("${module_name}/profiles/tests/${type}_check.sh.erb"),
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
  }

}
