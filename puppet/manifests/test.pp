##
# Define: prov_base::test
##
define prov_base::test {

  include prov_base::test::base

  file { "/usr/lib/prov/tests/${name}":
    source => "puppet:///modules/${module_name}/profiles/tests/${name}",
    owner  => 'root',
    group  => 'root',
    mode   => '755',
  }

}
