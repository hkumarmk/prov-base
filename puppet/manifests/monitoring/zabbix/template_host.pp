#
#
#
define prov_base:::monitoring::zabbix::template_host (
  $template = $name,
  $host = $::hostname,
) {

  zabbix_template_host{"${template}@${host}":
    ensure => present
  }
}
