#
# Class prov_base:::monitoring::zabbix::server
#
class prov_base:::monitoring::zabbix::server (

) {
  contain prov_base:::monitoring::zabbix::common
  contain ::zabbix::params
  contain ::zabbix::repo
  contain ::zabbix::server
  contain ::apache
  contain ::apache::mod::php
  contain ::zabbix::web

  if $zabbix::web::apache_use_ssl {
    $port = 443
  } else {
    $port = 80
  }

  prov_base:::test::check {'zabbix':
    ssl  => $zabbix::web::apache_use_ssl,
    port => $port,
  }
}
