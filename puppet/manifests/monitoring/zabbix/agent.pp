#
#
#
class prov_base:::monitoring::zabbix::agent (
  $hostgroup      = $::prov_role,
  $hostname       = $::hostname,
  $address        = $::ipaddress,
  $use_ip         = true,
  $port           = 10050,
  $templates      = ['Template OS Linux'],
  $proxy          = undef,
) inherits prov_base:::monitoring::zabbix::common {

  class {'::zabbix::agent':
    server => $zabbix_url
  }

  zabbix_host { $hostname:
    ipaddress      => $address,
    use_ip         => $use_ip,
    port           => $port,
    group          => $hostgroup,
    templates      => [],
    proxy          => $proxy,
  }

  zabbix_hostgroup {$hostgroup:
    ensure => present
  }

  prov_base:::monitoring::zabbix::template_host {$templates:}

}
