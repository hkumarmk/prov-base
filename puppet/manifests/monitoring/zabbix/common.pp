#
# prov_base:::monitoring::zabbix::common
#
class prov_base:::monitoring::zabbix::common (
  $zabbix_pass,
  $zabbix_url     = '127.0.0.1',
  $zabbix_user    = 'admin',
  $apache_use_ssl = false,
) {

  ensure_packages('ruby1.9.1-dev')

  package {'zabbixapi':
    ensure   => present,
    provider => 'gem',
    require  => Package['ruby1.9.1-dev'],
  }

  Zabbix_template_host {
    zabbix_url     => $zabbix_url,
    zabbix_user    => $zabbix_user,
    zabbix_pass    => $zabbix_pass,
    apache_use_ssl => $apache_use_ssl,
  }

  Zabbix_host {
    zabbix_url     => $zabbix_url,
    zabbix_user    => $zabbix_user,
    zabbix_pass    => $zabbix_pass,
    apache_use_ssl => $apache_use_ssl,
  }

  Zabbix_hostgroup {
    zabbix_url     => $zabbix_url,
    zabbix_user    => $zabbix_user,
    zabbix_pass    => $zabbix_pass,
    apache_use_ssl => $apache_use_ssl,
  }

  Zabbix_template {
    zabbix_url     => $zabbix_url,
    zabbix_user    => $zabbix_user,
    zabbix_pass    => $zabbix_pass,
    apache_use_ssl => $apache_use_ssl,
  }

  Zabbix_template<||> -> Zabbix_template_host<||>
  Zabbix_host<||> -> Zabbix_template_host<||>

}
