#
# Monitoring agent
#
class prov_base:::monitoring::agent (
  $tool = 'zabbix',
) {
  if $tool == 'zabbix' {
    contain prov_base:::monitoring::zabbix::agent
  } else {
    warning("Unknown montoring tool - $tool")
  }

}
