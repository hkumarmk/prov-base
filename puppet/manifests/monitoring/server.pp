#
# Monitoring server
#
class prov_base:::monitoring::server (
  $tool = 'zabbix',
) {
  if $tool == 'zabbix' {
    contain prov_base:::monitoring::zabbix::common
    contain prov_base:::monitoring::zabbix::server
    contain prov_base:::monitoring::zabbix::agent
  } else {
    fail("Unknown montoring tool - $tool")
  }

}
