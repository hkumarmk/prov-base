#
#
#
define prov_base:::monitor::service (
  $service_name	      = $name,
  $user_param_source  = undef,
  $user_param_content = undef,
  $template           = undef,
  $template_source    = undef,
  $host_name          = $::hostname,
) {

  include prov_base:::monitoring::zabbix::common

  zabbix::userparameters { $service_name:
    source  => $user_param_source,
    content => $user_param_content,
  }

  if $template {
    zabbix_template_host{"${template}@${host_name}":
      ensure => present
    }

    if $template_source {
      zabbix_template { $template:
        template_source => $template_source,
      }
    }
  }

}
