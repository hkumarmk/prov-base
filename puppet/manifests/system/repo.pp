#
# Class prov_base::system::repo
#
#
class prov_base::system::repo (
  $all_repos    = {},
  $active_repos = [],
  $keys         = {},
){
  case $::osfamily {
    'Debian': {

      contain ::apt

      Apt::Source<||> -> Package<||>

      create_resources(prov_base::system::repo::apt, $all_repos, { active_repos => $active_repos })
      create_resources(::apt::key, $keys,{})
    }
    'RedHat': {

      Yumrepo<||> -> Package<||>

      create_resources(prov_base::system::repo::yum, $all_repos, { active_repos => $active_repos })
    }
    default: {
      fail("OS family ${::osfamily} not supported")
    }
  }
}
