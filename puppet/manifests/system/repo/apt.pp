#
# Define: prov_base::system::repo::apt
#

define prov_base::system::repo::apt (
  $location,
  $repos          = 'main',
  $key            = undef,
  $key_server     = undef,
  $key_source     = undef,
  $key_content    = undef,
  $include_src    = false,
  $include 	  = undef,
  $architecture   = 'amd64',
  $release        = 'trusty',
  $active_repos   = [],
  $pin            = undef,
  $comment        = undef,
) {
  if member($active_repos,$name) {
    ::apt::source { $name:
      location     => $location,
      repos        => $repos,
      include_src  => $include_src,
      include	   => $include,
      architecture => $architecture,
      release      => $release,
      key          => $key,
      key_server   => $key_server,
      key_source   => $key_source,
      key_content  => $key_content,
      pin          => $pin,
      comment      => $comment,
    }
  }
}
