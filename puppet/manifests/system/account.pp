#
# Class: prov_base::system::account
# Purpose is to group all system user account actions in single class
#
# == Parameters
#
# [* local_users *]
#   A hash containing all users' details.
#
# [* active_users *]
#   Array of all active users
#
# [* sudo_users *]
#   Array of all sudo users
#   TODO: currently sudo configuratin is not flexible enough to filter the
#   access, sudo_users will get all access
#
# [* root_password *]
#   Md5 hashed root user password, by default root account is disabled.
#


class prov_base::system::account (
  $active_users  = [],
  $sudo_users    = [],
  $local_users   = {},
  $root_password = '*',
) {

  ## setup root user password.
  user { 'root':
    name     => 'root',
    ensure   => present,
    password => $root_password,
  }

  ## Add accounts for all local users which are set as active
  ## The idea is to specify details of all users and
  ## override the users in different hierarichy like
  ## environment wide, role wide etc

  create_resources('prov_base::system::account::instance',$local_users,{active_users => $active_users})

  ## setup sudoers
  class { 'sudo':
    purge => false,
    config_file_replace => false,
  }

  ## Make an intersection of active users and sudo users,
  ##  so that sudo_users are always a subset of active_users

  $sudo_users_orig = intersection($active_users,$sudo_users)

  prov_base::system::account::sudo_conf { $sudo_users_orig: }

}
