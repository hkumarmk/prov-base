#!/bin/bash
#

if [[ -z $LOG_CONSOLE ]]; then
  log="--logdest=syslog"
fi

function run_puppet {
    puppet apply --detailed-exitcodes ${log} $(puppet config print manifest); ret_code=$?
    if [[ $ret_code = 1 || $ret_code = 4 || $ret_code = 6 ]]; then
        echo "`date` | Puppet failed with return code ${ret_code}, trying again"
        puppet apply --detailed-exitcodes ${log} $(puppet config print manifest); ret_code=$?
        if [[ $ret_code = 1 || $ret_code = 4 || $ret_code = 6 ]]; then
            echo "`date` | Puppet failed again with return code ${ret_code}"
        fi
    else
        echo "`date` | Successfully executed puppet"
    fi
}

echo "`date` | Running validation"
run-parts --regex=. --verbose --exit-on-error  --report /usr/lib/prov/tests/ ; ret_code=$?
if [ $ret_code -ne 0 ]; then
    echo "`date` | Validation failed, Running puppet"
    run_puppet
elif [[ -n $RUN_PUPPET ]]; then
    echo "`date` | Running puppet"
    run_puppet
fi
