#!/bin/bash
set -e
function fail {
  echo "CRITICAL: $@"
  exit 2
}

if [ -f /root/openrc ]; then
  source /root/openrc
  heat stack-list || fail 'stack list failed'
else
  echo 'Critical: Openrc does not exist'
  exit 2
fi
